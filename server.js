const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');


const app = express()
app.use(cors())
app.use(express.json());


app.get('/', (req, res) =>{
    res.send('Hello world');
});


app.use('/api', require('./routes/auth'));




const port = 4000;

app.listen(port, () => console.log(`Example app listening on port ${port}!`))