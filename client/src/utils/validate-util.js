

export const validation = (firstName) => {
    let  message = null;
    let err =  true;
    if(firstName.trim() === ''){
        message = 'Enter Field'
        err = true;
       
    } else if (firstName.length < 1 ){
        message = 'First name must have min. 1 character';
        err = true;
       
    }else{
       message =  null;
       err = false;
    }

    return  {
        message,
        err
    }
}