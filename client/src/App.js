import React from 'react';
import { Provider } from 'react-redux';
import store from './store';

import './App.css';
import SignUp from './components/SignUp';

function App() {
  return (
      <Provider store={store} >
         <div className="App">
            <SignUp />
        </div>
    </Provider>
  );
}

export default App;
