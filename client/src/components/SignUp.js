import React, {useState, useEffect, Fragment } from 'react';
import { connect } from 'react-redux';
import { register } from '../actions/authAction';

const SignUp = ({register, registrationStatus}) => {
    const [newUser, setNewUser ] =  useState({
       firstName:"",
       lastName:"",
       address:"",
       phone:"",
       email:"",
       reciveNewsLetter:true
    });

    const [errorMsg, setErrorMsg] = useState({
        firstName: null,
        lastName: null,
        address: null,
        phone: null,
        email: null,
        reciveNewsLetter: false
        

    });

    const [ isDisabled, setIsDisabled ] = useState(true);
    const { firstName , lastName, address, phone, email, reciveNewsLetter } = newUser;
  

    useEffect(() =>{
         if(firstName.length < 1 || lastName.length < 1 || address.length < 1 || phone.length < 1 ||  email === "" ){
           
            setIsDisabled(true);
        }else {// eslint-disable-next-line
            let errorValues = Object.values(errorMsg).filter(item => {
                if(item !== null && item !== false){
                    return item;
                }
            });
    
           errorValues.length > 0 ?   setIsDisabled(true):   setIsDisabled(false);
     
        }
        
         // eslint-disable-next-line
    }, [newUser, errorMsg]);


    const handleChange = (e) => {
      
        const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
    
        if (e.target.type === 'checkbox' ){
            setNewUser({ ...newUser, reciveNewsLetter: value })
           
        }else{
            setNewUser({ ...newUser, [e.target.name]: value })
            
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();
         register(newUser);

         setNewUser({
            firstName:"",
            lastName:"",
            address:"",
            phone:"",
            email:"",
            reciveNewsLetter:true
         });

         setErrorMsg({
            firstName: null,
            lastName: null,
            address: null,
            phone: null,
            email: null,
          
         })
         setIsDisabled(true);
    }
 

    const handleBlur = (e) =>{
     
        if (e.target.name === 'firstName' ){
            if(firstName === ''){
                setErrorMsg({ ...errorMsg, firstName: 'Field is required' })
                setIsDisabled(true)
            }else{
                setErrorMsg({ ...errorMsg, firstName: null})
            }
     
        }
        if(e.target.name === 'lastName'  ){
            if(lastName.length < 1){
               setErrorMsg({ ...errorMsg, lastName: 'Field is required' })
               setIsDisabled(true)
            }else {
                setErrorMsg({ ...errorMsg, lastName: null})
 
            }
        }

        if(e.target.name === 'address'  ){
            if(address.length < 1){
               setErrorMsg({ ...errorMsg, address: 'Field is required' })
               setIsDisabled(true)
            }else {
                setErrorMsg({ ...errorMsg, address: null})
    
            }
        }

        if(e.target.name === 'email'){

            const pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;  //eslint-disable-line
            if(email.length < 1){
                setErrorMsg({...errorMsg,email:'Field is required'})
                setIsDisabled(true)
            } else if(!email.match(pattern)){
                setErrorMsg({...errorMsg,email:'Bad format'})
                setIsDisabled(true)
            } else {
                setErrorMsg({ ...errorMsg, email: null})
            }
        }

        if(e.target.name === 'phone'){
            const pattern = /^\+(?:[0-9] ?){5,18}[0-9]$/g;
             
            if(phone.length < 1  ){
                setErrorMsg({...errorMsg,phone:'Field is required'})
                setIsDisabled(true)
            } 
            else if(!phone.match(pattern)){
                setErrorMsg({...errorMsg,phone:'Phone format must be: +xx xxxx xxxx'})
                setIsDisabled(true)
            }else{
                setErrorMsg({ ...errorMsg, phone: null})
        
            }
        }
    }

    return (
        <Fragment>
        <form onSubmit={handleSubmit}>
            <div className="container">
                <h1>Sign Up</h1>
                <p>Please fill in this form to create an account.</p> 
                <div className={`input-section ${errorMsg.firstName ? "error": ""}`} >
                    <label htmlFor="firstName">First Name</label>
                    <input type="text"
                        name="firstName"
                        value={firstName}
                        placeholder="FIrst Name"
                        onChange={handleChange}  
                        onBlur={handleBlur} />
                    {errorMsg.firstName && 
                    <div className="error-message" style={{color:'red'}}>{errorMsg.firstName}</div> }
            </div>
                <div className={`input-section ${errorMsg.lastName ? "error" : ""}`} >
                    <label htmlFor="lastName">Last Name</label>
                    <input type="text"
                        name="lastName"
                        value={lastName}
                        placeholder="lastName"
                        onChange={handleChange}
                        onBlur={handleBlur} />
                    {errorMsg.lastName &&
                        <div className="error-message" style={{ color: 'red' }}>{errorMsg.lastName}</div>}
                </div>
                <div className={`input-section ${errorMsg.address ? "error" : ""}`} >
                    <label htmlFor="address">Address </label>
                    <input type="text"
                        name="address"
                        value={address}
                        placeholder="address"
                        onChange={handleChange}
                        onBlur={handleBlur} />
                    {errorMsg.address &&
                        <div className="error-message" style={{ color: 'red' }}>{errorMsg.address}</div>}
                </div>
                <div className={`input-section ${errorMsg.email ? "error" : ""}`} >
                    <label htmlFor="email">Email </label>
                    <input type="text"
                        name="email"
                        value={email}
                        placeholder="E-mail"
                        onChange={handleChange}
                        onBlur={handleBlur} />
                    {errorMsg.email &&
                        <div className="error-message" style={{ color: 'red' }}>{errorMsg.email}</div>}
                </div>
                <div className={`input-section ${errorMsg.phone ? "error" : ""}`} >
                    <label htmlFor="phone">Phone Num</label>
                    <input type="text"
                        name="phone"
                        value={phone}
                        placeholder="Phone: +xxxxxxxx"
                        onChange={handleChange}
                        onBlur={handleBlur} />
                    {errorMsg.phone &&
                        <div className="error-message" style={{ color: 'red' }}>{errorMsg.phone}</div>}
                </div>
                <div className="input-section" >
                    <label htmlFor="phone">Recive news Letter</label>
                    <input type="checkbox"
                        checked={reciveNewsLetter}
                        value={reciveNewsLetter}
                        onChange={handleChange}
                        />
                </div>

                 <button type="submit" disabled={isDisabled}>Submit</button>
            </div>
        </form>
        </Fragment>
    )
}

const mapStateToProps = state => ({
    registrationStatus: state.value
  });
export default connect(mapStateToProps, { register})(SignUp)
