import {
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    SET_REGISTRATION_STATUS,
    SET_LOADING

  } from "../actions/types";
  
  const initialState = {
      users:[],
      currentUser:null,
      loading:false,
      status:null,
      registrationStatus:false,

  }
  export default (state = initialState, action) =>{
      switch(action.type){
        case REGISTER_SUCCESS:
          //  localStorage.setItem('token', action.payload.token);
            return {
                ...state,
                users: [...state.users,action.payload ],
                currentUser:action.payload,
                isAuthenticated: true,
                loading: false,
                registrationStatus:true
            };
          case SET_LOADING:
             return {
                 ...state,
                 loading:true
             }
          case REGISTER_FAIL:
            console.error(action.payload);
            return {
                ...state,
                status:action.payload,
                error:true
            }
          case SET_REGISTRATION_STATUS:
            return {
              ...state,
              registrationStatus:false
            }
          default:
          return state;
      }
  }