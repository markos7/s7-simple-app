import axios from 'axios';

import {
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    SET_REGISTRATION_STATUS,
    SET_LOADING
  } from "./types";
  
const baseURL = "http://localhost:4000";
  
  export const register =  (newUser) => async (dispatch) => {

    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

   try {
        const result =  await axios.post(`${baseURL}/api/register`, newUser, config);
        const data =   await result.data;
        console.log(data);
        dispatch({
            type: REGISTER_SUCCESS,
            payload: data.response
   
        })
   } catch (error) {
            dispatch({
                type: REGISTER_FAIL,
                payload: error.response
            });

   }

};

  export const setRegistrationStatus =()  => dispatch => {
    dispatch({
        type: SET_REGISTRATION_STATUS,
    });
  } 
  // Set Loading
  export const setLoading = () => {
      return {
          type:SET_LOADING
      }
  }